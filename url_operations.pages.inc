<?php

/**
 * @file url_operations.pages.inc
 *
 * Provides page callback functions for the URL Operations module.
 */


/**
 * Page callback for the operation URL
 */
function url_operations_operate($entity_type, $entity, $op, $user) {
  // Watchdog for reference
  watchdog('url_operations', 'Operation %op requested for %entity_type (%entity_id)', array(
    '%op' => $op,
    '%entity_type' => $entity_type,
    '%entity_id' => url_operations_entity_id($entity_type, $entity),
  ));

  // Invoke hook_url_operations_operate_OP - where 'hook' is the module providing the entity type
  if ($info = module_invoke($entity_type, 'url_operations_operate_' . $op, $entity, $user)) {
    return drupal_get_form('url_operations_operate_confirm', $info, $entity, $user);
  }

  // Fallback to a 404
  return MENU_NOT_FOUND;
}


/**
 * Confirm form for an operation
 */
function url_operations_operate_confirm($form, &$form_state, $info, $entity, $user) {
  // Store the three parameters in the form for reference
  $form['op_info'] = array('#type' => 'value', '#value' => $info);
  $form['entity']  = array('#type' => 'value', '#value' => $entity);
  $form['user']    = array('#type' => 'value', '#value' => $user);

  return confirm_form($form, $info['question'], $info['path']);
}


/**
 * Submit handler for the confirm form
 */
function url_operations_operate_confirm_submit(&$form, &$form_state) {
  // Retreive these items for convenience
  $info   = &$form_state['values']['op_info'];
  $entity = &$form_state['values']['entity'];
  $user   = &$form_state['values']['user'];

  // Run the actions
  $result = actions_do($info['actions'], $entity, array(
    'hook' => 'url_operations_operate',
    'group' => 'url_operations',
    'user' => $user,
  ));

  if (url_operations_setting('url_operations_display_message') && isset($info['message'])) {
    drupal_set_message($info['message']);
  }

  // Redirect, if set for the operation
  $form_state['redirect'] = isset($info['redirect']) ? $info['redirect'] : '';
}


/**
 * Callback to generate links for a given entity type and entity
 */
function url_operations_links($entity_type, $entity) {
  // We use this more than once - more efficient?
  $is_admin = user_access('administer url operations');

  // Retreive the User ID from the $_GET parameter, if we are allowed to.
  // Otherwise, fallback to the current logged in user
  $user = (isset($_GET['uid']) && $is_admin) ? user_load($_GET['uid']) : user_uid_optional_load();

  // Get links for this context
  $links = url_operations_generate_links($entity_type, $entity, $user);

  // Generate the swith user form, if we're allowed
  // TODO - Is render() ok?
  $user_form = $is_admin ? render(drupal_get_form('url_operations_links_user', $user)) : '';

  // Prettify the username
  $name = ($user->uid > 0) ? $user->name : variable_get('anonymous', t('Anonymous'));

  // Return as a themed list of links
  return $user_form . theme('links', array(
    'heading' => array(
      'text' => t('Operation Links for: @name', array('@name' => $name)),
      'level' => 'h2',
      'class' => '',
    ),
    'links' => $links,
    'attributes' => array(
      'class' => 'url-operations-link-list',
    ),
  ));
}


/**
 * Switch user form for the links page
 */
function url_operations_links_user($form, &$form_state, $user) {
  $form['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Switch User'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  $form['wrapper']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('Generate URLs for:'),
    '#default_value' => $user->name,
    '#maxlength' => 60,
    '#size' => 30,
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
    '#autocomplete_path' => 'user/autocomplete',
  );

  $form['wrapper']['actions'] = array('#type' => 'actions');
  $form['wrapper']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Switch'),
    '#submit' => array('url_operations_links_user_switch_submit'),
  );
  $form['wrapper']['actions']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('url_operations_links_user_reset_submit'),
  );


  return $form;
}


/**
 * Submit handler - basically redirects to the same page with a uid=X query string
 */
function url_operations_links_user_switch_submit($form, &$form_state) {
  if ($user = user_load_by_name($form_state['values']['user'])) {
    $form_state['redirect'] = array(current_path(), array('query' => array('uid' => $user->uid)));
  }
}


/**
 * Submit handler - reset the uid=X parameter, this forces the form and links to
 * return to the current user.
 */
function url_operations_links_user_reset_submit($form, &$form_state) {
  $form_state['redirect'] = array(current_path());
}
