<?php

/**
 * Implements hook_url_operations_operate_publish()
 */
function user_url_operations_operate_block($context_user, $user) {
  // In this case, $context_user is the one being operated ON.
  // $user is the ACTING user
  if ($context_user->uid == 0) {
    $context_user->name = variable_get('anonymous', t('Anonymous'));
  }

  return array(
    'question' => t('Block %name now?', array('%name' => $context_user->name)),
    'actions' => array('user_block_user_action'),
    'message' => t('%name was blocked', array('%name' => $context_user->name)),
  ) + _user_url_operations_operate_common($context_user);
}


/**
 * Internal function for common user operation info
 */
function _user_url_operations_operate_common($user) {
  return array(
    'path' => 'user/' . $user->uid,
    'redirect' => 'user/' . $user->uid,
  );
}


/**
 * Implements hook_url_operations_links()
 */
function user_url_operations_links($context_user, $uid = NULL, $only_ops = NULL) {
  // $context_user is the user being acted ON. $uid / $user is the ACTING user.
  // Get either the provided user, or the current one
  $user = user_uid_optional_load($uid);

  // Get the info for this supported module
  $info = url_operations_get_supported_modules('user');

  // Define the available options
  $ops = isset($only_ops) ? array_intersect_key($info['operations'], $only_ops) : $info['operations'];

  // Build array of links
  $links = array();
  foreach ($ops as $op => $op_info) {
    $links[$op] = url_operations_generate_link($op_info['title'], 'user', $context_user, $op, $user->uid, REQUEST_TIME);
  }

  // Return
  return $links;
}


/**
 * Implements hook_url_operations_entity_access()
 */
function user_url_operations_entity_access($context_user) {
  // Can generate links if we can administer users OR we can generate links, can access profiles and the user is active.
  // TODO - this potentially means a user could generate a link to block admin! Is this ok?! Mitigated by permissions?
  return user_access('administer users') ||
        (user_access('generate user url operations') && user_access('access user profiles') && $context_user->status);
}


/**
 * Implements hook_url_operations_permission()
 */
function user_url_operations_permission() {
  return array(
    'generate user url operations' => array(
      'title' => t('Generate URL Operation links for users'),
      'description' => t('Allow users to generate one-time links for all users'),
    ),
  );
}


/**
 * Implements hook_url_operations_menu()
 */
function user_url_operations_menu() {
  $items = array();

  // user/{uid}/url-operations/{op}/{uid}/{timestamp}/{hash}
  $items['user/%user/url-operations/%/%user/%/%'] = array(
    'title' => 'Operation',
    'page callback' => 'url_operations_operate',
    'page arguments' => array('user', 1, 3, 4, ),
    'access callback' => 'url_operations_operate_access',
    'access arguments' => array('user', 1, 3, 4, 5, 6),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['user/%user/url-operations'] = array(
    'title' => 'URL Operations',
    'page callback' => 'url_operations_links',
    'page arguments' => array('user', 1),
    'access callback' => 'url_operations_links_access',
    'access arguments' => array('user', 1),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}
