<?php

/**
 * Implements hook_url_operations_operate_publish()
 */
function comment_url_operations_operate_publish($comment, $user) {
  return array(
    'question' => t('Publish %title now?', array('%title' => $comment->subject)),
    'actions' => array('comment_publish_action', 'comment_save_action'),
    'message' => t('%title was published', array('%title' => $comment->subject)),
  ) + _comment_url_operations_operate_common($comment);
}


/**
 * Implements hook_url_operations_operate_unpublish()
 */
function comment_url_operations_operate_unpublish($comment, $user) {
  return array(
    'question' => t('Unpublish %title now?', array('%title' => $comment->subject)),
    'actions' => array('comment_unpublish_action', 'comment_save_action'),
    'message' => t('%title was unpublished', array('%title' => $comment->subject)),
  ) + _comment_url_operations_operate_common($comment);
}


/**
 * Internal function for commone comment operation info
 */
function _comment_url_operations_operate_common($comment) {
  return array(
    'path' => 'comment/' . $comment->cid,
    'redirect' => 'comment/' . $comment->cid,
  );
}


/**
 * Implements hook_url_operations_links()
 */
function comment_url_operations_links($comment, $uid = NULL, $only_ops = NULL) {
  // Get either the provided user, or the current one
  $user = user_uid_optional_load($uid);

  // Get the info for this supported module
  $info = url_operations_get_supported_modules('comment');

  // Define the available options
  $ops = isset($only_ops) ? array_intersect_key($info['operations'], $only_ops) : $info['operations'];

  // Build array of links
  $links = array();
  foreach ($ops as $op => $op_info) {
    $links[$op] = url_operations_generate_link($op_info['title'], 'comment', $comment, $op, $user->uid, REQUEST_TIME);
  }

  // Return
  return $links;
}


/**
 * Implements hook_url_operations_entity_access()
 */
function comment_url_operations_entity_access($comment) {
  // Only allow if the user can admin comments OR can generate links, can view them and its published
  return user_access('administer comments') ||
        (user_access('generate comment url operations') && user_access('access comments') && $comment->status);
}


/**
 * Implements hook_url_operations_permission()
 */
function comment_url_operations_permission() {
  return array(
    'generate comment url operations' => array(
      'title' => t('Generate URL Operation links for comments'),
      'description' => t('Allow users to generate one-time links for all comments'),
    ),
  );
}


/**
 * Implements hook_url_operations_menu()
 */
function comment_url_operations_menu() {
  $items = array();

  // comment/{nid}/url-operations/{op}/{uid}/{timestamp}/{hash}
  $items['comment/%comment/url-operations/%/%user/%/%'] = array(
    'title' => 'Operation',
    'page callback' => 'url_operations_operate',
    'page arguments' => array('comment', 1, 3, 4, ),
    'access callback' => 'url_operations_operate_access',
    'access arguments' => array('comment', 1, 3, 4, 5, 6),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['comment/%comment/url-operations'] = array(
    'title' => 'URL Operations',
    'page callback' => 'url_operations_links',
    'page arguments' => array('comment', 1),
    'access callback' => 'url_operations_links_access',
    'access arguments' => array('comment', 1),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}
