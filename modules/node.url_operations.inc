<?php

/**
 * Implements hook_url_operations_operate_publish()
 */
function node_url_operations_operate_publish($node, $user) {
  return array(
    'question' => t('Publish %title now?', array('%title' => $node->title)),
    'actions' => array('node_publish_action', 'node_save_action'),
    'message' => t('%title was published', array('%title' => $node->title)),
  ) + _node_url_operations_operate_common($node);
}


/**
 * Implements hook_url_operations_operate_unpublish()
 */
function node_url_operations_operate_unpublish($node, $user) {
  return array(
    'question' => t('Unpublish %title now?', array('%title' => $node->title)),
    'actions' => array('node_unpublish_action', 'node_save_action'),
    'message' => t('%title was unpublished', array('%title' => $node->title)),
  ) + _node_url_operations_operate_common($node);
}


/**
 * Internal function for commone node operation info
 */
function _node_url_operations_operate_common($node) {
  return array(
    'path' => 'node/' . $node->nid,
    'redirect' => 'node/' . $node->nid,
  );
}


/**
 * Implements hook_url_operations_links()
 */
function node_url_operations_links($node, $uid = NULL, $only_ops = NULL) {
  // Get either the provided user, or the current one
  $user = user_uid_optional_load($uid);

  // Get the info for this supported module
  $info = url_operations_get_supported_modules('node');

  // Define the available options
  $ops = isset($only_ops) ? array_intersect_key($info['operations'], $only_ops) : $info['operations'];

  // Build array of links
  $links = array();
  foreach ($ops as $op => $op_info) {
    $links[$op] = url_operations_generate_link($op_info['title'], 'node', $node, $op, $user->uid, REQUEST_TIME);
  }

  // Return
  return $links;
}


/**
 * Implements hook_url_operations_entity_access()
 */
function node_url_operations_entity_access($node) {
  // Only allow if the user can admin nodes OR can generate links and has permission to view the node
  // TODO should this be view or edit?
  return user_access('administer nodes') ||
        (user_access('generate node url operations') && node_access('view', $node));
}


/**
 * Implements hook_url_operations_permission()
 */
function node_url_operations_permission() {
  return array(
    'generate node url operations' => array(
      'title' => t('Generate URL Operation links for nodes'),
      'description' => t('Allow users to generate one-time links for all nodes'),
    ),
  );
}


/**
 * Implements hook_url_operations_menu()
 */
function node_url_operations_menu() {
  $items = array();

  // node/{nid}/url-operations/{op}/{uid}/{timestamp}/{hash}
  $items['node/%node/url-operations/%/%user/%/%'] = array(
    'title' => 'Operation',
    'page callback' => 'url_operations_operate',
    'page arguments' => array('node', 1, 3, 4, ),
    'access callback' => 'url_operations_operate_access',
    'access arguments' => array('node', 1, 3, 4, 5, 6),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/url-operations'] = array(
    'title' => 'URL Operations',
    'page callback' => 'url_operations_links',
    'page arguments' => array('node', 1),
    'access callback' => 'url_operations_links_access',
    'access arguments' => array('node', 1),
    'file' => 'url_operations.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}
