<?php

/**
 * Admin settings form
 */
function url_operations_admin_settings_form() {
  $form = array();

  // Borrowed from system_performance_settings
  $period = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400), 'format_interval');
  $period[0] = '<' . t('none') . '>';

  $form['url_operations_link_expire'] = array(
    '#title' => t('Link Lifespan'),
    '#description' => t('How long should a link be valid for?'),
    '#type' => 'select',
    '#options' => $period,
    '#required' => TRUE,
    '#default_value' => url_operations_setting('url_operations_link_expire'),
  );


  $form['url_operations_display_message'] = array(
    '#title' => t('Display message after running the URL?'),
    '#type' => 'checkbox',
    '#default_value' => url_operations_setting('url_operations_display_message'),
  );

  return system_settings_form($form);
}
